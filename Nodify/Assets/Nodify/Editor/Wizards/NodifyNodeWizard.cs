﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Nodify.Editor
{
    public class NodifyNodeWizard : EditorWindow
    {
        private string menuPath = "Unity/GameObject/Do Thing";
        private string displayName = "GameObject.DoThing";
        private string iconResourcePath = string.Empty;
        private bool openInEditor = false;

        private void OnGUI()
        {
            GUILayout.Label("Node Wizard", EditorStyles.largeLabel);

            menuPath = EditorGUILayout.TextField("Context Menu Path", menuPath);
            displayName = EditorGUILayout.TextField("Display Name", displayName);
            iconResourcePath = EditorGUILayout.TextField("Icon Resource Path [optional]", iconResourcePath);
            openInEditor = EditorGUILayout.Toggle("Open in Editor", openInEditor);

            string className = menuPath.Replace("/", string.Empty).Replace(" ", string.Empty).Replace(".", string.Empty);

            if(GUILayout.Button("Create", EditorStyles.toolbarButton))
            {
                TextAsset template = Resources.Load<TextAsset>("Templates/NodifyNewNodeTemplate");

                if(template != null)
                {
                    string codeTemplate = template.text;

                    codeTemplate = codeTemplate.Replace("{menuPath}", menuPath);
                    codeTemplate = codeTemplate.Replace("{className}", className);
                    codeTemplate = codeTemplate.Replace("{displayName}", displayName);

                    if(!string.IsNullOrEmpty(iconResourcePath))
                    {
                        char s = '"';
                        codeTemplate = codeTemplate.Replace("{icon}", ", " + s + iconResourcePath + s);
                    }
                    else
                    {
                        codeTemplate = codeTemplate.Replace("{icon}", string.Empty);
                    }

                    string path = NodifyEditorUtilities.GetCurrentAssetPath();

                    path += className + ".cs";

                    System.IO.File.WriteAllText(path, codeTemplate);

                    AssetDatabase.Refresh();

                    if (openInEditor)
                    {
                        AssetDatabase.OpenAsset(AssetDatabase.LoadAssetAtPath(path, typeof(TextAsset)));
                    }

                    this.Close();
                }
            }
        }
    }
}
