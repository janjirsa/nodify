﻿using UnityEngine;
using System.Collections;

namespace Nodify.Runtime.Nodes
{
    [CreateMenu("Unity/Input/Key", "Input.Key")]
    public class UnityInputGetKey : Node
    {
        [Expose]
        public KeyCode keyCode;

        [Expose(true)]
        public void OnTrue()
        {
            
        }

        [Expose]
        public void OnFalse()
        {
            
        }

        protected override void OnExecute()
        {
             if(Input.GetKey(keyCode))
             {
                 this.Fire("OnTrue");
             }
             else
             {
                 this.Fire("OnFalse");
             }

 	         base.OnExecute();
        } 
    }
}