﻿using UnityEngine;
using System.Collections;

namespace Nodify.Runtime.Nodes
{
    [CreateMenu("Unity/Input/Key Down", "Input.KeyDown")]
    public class UnityInputGetKeyDown : Node
    {
        [Expose]
        public KeyCode keyCode;

        [Expose(true)]
        public void OnTrue()
        {
            
        }

        [Expose]
        public void OnFalse()
        {
            
        }

        protected override void OnExecute()
        {
             if(Input.GetKeyDown(keyCode))
             {
                 this.Fire("OnTrue");
             }
             else
             {
                 this.Fire("OnFalse");
             }

 	         base.OnExecute();
        } 
    }
}