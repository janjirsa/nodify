﻿using UnityEngine;
using System.Collections;

namespace Nodify.Runtime.Nodes
{
    [CreateMenu("Unity/Collider/Trigger", "Collider.Trigger", "Icons/unity_trigger_icon")]
    public class UnityTrigger : Node
    {
        [Expose]
        public GameObject target;

        [Expose]
        public Collider otherCollider;

        private ManagedBehaviour m_target;

        private void Awake()
        {
            m_target = (target.GetComponent<ManagedBehaviour>()) ? target.GetComponent<ManagedBehaviour>() : target.AddComponent<ManagedBehaviour>();

            m_target.TriggerEntered += delegate(Collider arg)
            {
                otherCollider = arg;
                this.Fire("OnTriggerDidEnter");
            };

            m_target.TriggerStayed += delegate(Collider arg)
            {
                otherCollider = arg;
                this.Fire("OnTriggerDidStay");
            };
            m_target.TriggerExited += delegate(Collider arg)
            {
                otherCollider = arg;
                this.Fire("OnTriggerDidLeave");
            };
        }

        [Expose(true)]
        public void OnTriggerDidEnter()
        {

        }

        [Expose]
        public void OnTriggerDidLeave()
        {

        }

        [Expose]
        public void OnTriggerDidStay()
        {

        }
    }
}
