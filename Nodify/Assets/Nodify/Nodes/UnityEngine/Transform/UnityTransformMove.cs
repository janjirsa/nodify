using UnityEngine;
using System.Collections;
using Nodify.Runtime;

namespace Nodify.Runtime.Nodes
{
	[CreateMenu("Unity/Transform/Move", "Transform.Move")]
	public class UnityTransformMove : Node 
	{
        [Expose]
        public Transform target;

        [Expose]
        public Vector3 direction;

        [Expose]
        public bool useDeltaTime;

        [Expose]
        public bool relative;

		protected override void OnExecute()
		{
			Vector3 targetDir = direction;

			if(relative)
			{
				targetDir = target.TransformDirection(targetDir);
			}

            if (useDeltaTime)
            {
                target.position += targetDir * Time.deltaTime;
            }
            else
            {
                target.position += targetDir;
            }

			base.OnExecute();
		}
	}
}
