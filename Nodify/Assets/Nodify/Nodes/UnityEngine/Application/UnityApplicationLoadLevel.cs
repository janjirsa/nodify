#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using Nodify.Runtime;

namespace Nodify.Runtime.Nodes
{
	[CreateMenu("Unity/Application/Load Level", "Application.LoadLevel" )]
	public class UnityApplicationLoadLevel : Node 
	{
        public enum LoadLevelType
        {
            LoadLevel,
            LoadLevelAdditive,
            LoadLevelAsync,
            LoadLevelAdditiveAsync
        }
        public enum LoadByType
        {
            Name,
            Id,
            Next,
            Back
        }

        [Expose][HideInInspector]
        public string levelName;
        [Expose][HideInInspector]
        public int levelId;

        [Expose]
        public LoadLevelType loadLevelType = LoadLevelType.LoadLevel;
        [Expose]
        public LoadByType loadByType = LoadByType.Name;

        private int GetLevelId(LoadByType type) 
        {
            if (type == LoadByType.Id) return levelId;
            if (type == LoadByType.Next) return Application.loadedLevel + 1;
            if (type == LoadByType.Back) return Application.loadedLevel - 1;
            return 0 ;
        }

		protected override void OnExecute()
		{
            switch (loadLevelType)
            {
                case LoadLevelType.LoadLevel:
                    if (loadByType == LoadByType.Name) { Application.LoadLevel(levelName); } else { Application.LoadLevel(GetLevelId(loadByType)); }
                    break;
                case LoadLevelType.LoadLevelAdditive:
                    if (loadByType == LoadByType.Name) { Application.LoadLevelAdditive(levelName); } else { Application.LoadLevelAdditive(GetLevelId(loadByType)); }
                    break;
                case LoadLevelType.LoadLevelAsync:
                    if (loadByType == LoadByType.Name) { Application.LoadLevelAsync(levelName); } else { Application.LoadLevelAsync(GetLevelId(loadByType)); }
                    break;
                case LoadLevelType.LoadLevelAdditiveAsync:
                    if (loadByType == LoadByType.Name) { Application.LoadLevelAdditiveAsync(levelName); } else { Application.LoadLevelAdditiveAsync(GetLevelId(loadByType)); }
                    break;
            }

			base.OnExecute();
		}

        #if UNITY_EDITOR
        public override void CustomInspectorGUI()
        {
            if (loadByType == LoadByType.Name) levelName = EditorGUILayout.TextField("Level Name", levelName); 
            if (loadByType == LoadByType.Id) levelId = EditorGUILayout.IntField("Level Name", levelId); 
        }
        #endif
	}
}
